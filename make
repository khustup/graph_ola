#! /bin/bash

export GCC=g++
export MOC=moc
export LD=g++
export AR=ar
export OBJ_PATH=./obj
export LIB_PATH=./lib
export BIN_PATH=./bin
export FLAGS=" -g "
export INCLUDE="-I/usr/lib/qt-3.3/include/ -I$PWD/src/"
export LIBS="-L/usr/lib/qt-3.3/lib -lqui"

###############################################################################################
# Project graph.
echo "$GCC $FLAGS -c src/graph/graph_generator.cpp -o $OBJ_PATH/graph/graph_generator.o $INCLUDE"
$GCC $FLAGS -c src/graph/graph_generator.cpp -o $OBJ_PATH/graph/graph_generator.o $INCLUDE

echo "$GCC $FLAGS -c src/graph/base_ola_engine.cpp -o $OBJ_PATH/graph/base_ola_engine.o $INCLUDE"
$GCC $FLAGS -c src/graph/base_ola_engine.cpp -o $OBJ_PATH/graph/base_ola_engine.o $INCLUDE

echo "$GCC $FLAGS -c src/graph/ola_line.cpp -o $OBJ_PATH/graph/ola_line.o $INCLUDE"
$GCC $FLAGS -c src/graph/ola_line.cpp -o $OBJ_PATH/graph/ola_line.o $INCLUDE

echo "$GCC $FLAGS -c src/graph/ola_engine.cpp -o $OBJ_PATH/graph/ola_engine.o $INCLUDE"
$GCC $FLAGS -c src/graph/ola_engine.cpp -o $OBJ_PATH/graph/ola_engine.o $INCLUDE

echo "$GCC $FLAGS -c src/graph/np_ola_engine.cpp -o $OBJ_PATH/graph/np_ola_engine.o $INCLUDE"
$GCC $FLAGS -c src/graph/np_ola_engine.cpp -o $OBJ_PATH/graph/np_ola_engine.o $INCLUDE

echo "$GCC $FLAGS -c src/graph/arrangement_generator.cpp -o $OBJ_PATH/graph/arrangement_generator.o $INCLUDE"
$GCC $FLAGS -c src/graph/arrangement_generator.cpp -o $OBJ_PATH/graph/arrangement_generator.o $INCLUDE



echo "$AR rcs lib/libgraph.a obj/graph/*.o"
$AR rcs $LIB_PATH/libgraph.a $OBJ_PATH/graph/*.o

###############################################################################################
# Project gui.
echo "cd src/gui"
cd src/gui
echo "$MOC main_window.hpp > moc_main_window.cpp"
$MOC main_window.hpp > moc_main_window.cpp

echo "cd ../../"
cd ../../

echo "$GCC $FLAGS -c src/gui/moc_main_window.cpp -o $OBJ_PATH/gui/moc_main_window.o $INCLUDE"
$GCC $FLAGS -c src/gui/moc_main_window.cpp -o $OBJ_PATH/gui/moc_main_window.o $INCLUDE
echo "$GCC $FLAGS -c src/gui/main_window.cpp -o $OBJ_PATH/gui/main_window.o $INCLUDE"
$GCC $FLAGS -c src/gui/main_window.cpp -o $OBJ_PATH/gui/main_window.o $INCLUDE
echo "$GCC $FLAGS -c src/gui/canvas.cpp -o $OBJ_PATH/gui/canvas.o $INCLUDE"
$GCC $FLAGS -c src/gui/canvas.cpp -o $OBJ_PATH/gui/canvas.o $INCLUDE
echo "$GCC $FLAGS -c src/gui/ola_window.cpp -o $OBJ_PATH/gui/ola_window.o $INCLUDE"
$GCC $FLAGS -c src/gui/ola_window.cpp -o $OBJ_PATH/gui/ola_window.o $INCLUDE
echo "$GCC $FLAGS -c src/gui/utility.cpp -o $OBJ_PATH/gui/utility.o $INCLUDE"
$GCC $FLAGS -c src/gui/utility.cpp -o $OBJ_PATH/gui/utility.o $INCLUDE

echo "$AR rcs lib/libgui.a obj/gui/*.o"
$AR rcs $LIB_PATH/libgui.a $OBJ_PATH/gui/*.o

###############################################################################################
# Project main.
echo "$GCC $FLAGS -c src/main/main.cpp -o $OBJ_PATH/main/main.o $INCLUDE"
$GCC $FLAGS -c src/main/main.cpp -o $OBJ_PATH/main/main.o $INCLUDE

echo "$GCC $FLAGS -c src/main/example.cpp -o $OBJ_PATH/main/example.o $INCLUDE"
$GCC $FLAGS -c src/main/example.cpp -o $OBJ_PATH/main/example.o $INCLUDE

###############################################################################################
# Linking
echo "$LD $OBJ_PATH/main/main.o $LIB_PATH/libgraph.a $LIB_PATH/libgui.a -o $BIN_PATH/graph $LIBS"
$LD $OBJ_PATH/main/main.o $LIB_PATH/libgraph.a $LIB_PATH/libgui.a -o $BIN_PATH/graph $LIBS

echo "$LD $OBJ_PATH/main/example.o $LIB_PATH/libgraph.a -o $BIN_PATH/example $LIBS"
$LD $OBJ_PATH/main/example.o $LIB_PATH/libgraph.a -o $BIN_PATH/example $LIBS
