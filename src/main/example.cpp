#include <graph/graph.hpp>
#include <graph/graph_generator.hpp>
#include <graph/ola_engine.hpp>
#include <graph/np_ola_engine.hpp>

#include <iostream>
#include <string>

void dump_graph(graph::int_graph::ptr g)
{
        std::cout << "---------------Graph Dump-------------" << std::endl;
        const graph::int_graph::vertices& vs = g->get_vertices();
        graph::int_graph::vertices::const_iterator i = vs.begin();
        while(i != vs.end()) {
                graph::int_vertex::ptr v = *i++;
                std::cout << v->get_label() << ": ";
                graph::int_vertex::vertices::const_iterator j = v->get_neighbours().begin();
                while(j != v->get_neighbours().end()) {
                        std::cout << (*j++)->get_label() << ", ";
                }
                std::cout << std::endl;
        }
        std::cout << "--------------------------------------" << std::endl;
}

void dump_arrangement(const graph::ola_line& o)
{
        const graph::ola_line::line& l = o.get_line();
        std::cout << "----------Arrangement-----------------" << std::endl;
        std::cout << "Vertex\t\tPosition" << std::endl;
        graph::ola_line::line::const_iterator k = l.begin();
        while(k != l.end()) {
                std::cout << k->first->get_label() << "\t\t" << k->second << std::endl;
                ++k;
        }
        std::cout << "--------------------------------------" << std::endl;
        std::cout << "Result: " << o.get_result() << std::endl;
}

int main()
{
        graph::graph_generator::initialize();

        graph::int_graph::ptr g = graph::graph_generator::get_instance().get(9);
        dump_graph(g);

        graph::ola_engine e(g);
        e.run();
        dump_arrangement(e.get_ola_line());

        graph::np_ola_engine n(g);
        n.run();
        dump_arrangement(n.get_ola_line());

        graph::graph_generator::uninitialize();

        return 0;
}
