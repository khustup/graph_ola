#include <graph/graph_generator.hpp>
#include <gui/main_window.hpp>

#include <qapplication.h>

int main(int argc, char** argv)
{
        graph::graph_generator::initialize();

        QApplication q(argc, argv);
        gui::main_window m(std::string("Graph OLA Tool"));
        q.setMainWidget(&m);
        m.show();
        int x = q.exec();

        graph::graph_generator::uninitialize();

        return x;
}
