/**
 * @file gui/canvas.cpp
 * @brief Implementation of class gui::canvas
 */

#include "canvas.hpp"

#include <math.h>

#include <qcolor.h>
#include <qbrush.h>
#include <qpen.h>

namespace gui {

void
canvas::draw_point(int x, int y, QString t)
{
        QCanvasEllipse* e = new QCanvasEllipse(8, 8, this);
        e->setX(x);
        e->setY(y);
        e->setZ(20);
        e->setBrush(QBrush(QColor(0, 255, 0)));
        e->show();

        if(!t.isEmpty()) {
                draw_text(x, y - 25, t, QColor(0, 255, 0));
        }
}

void
canvas::draw_text(int x, int y, QString t, QColor c)
{
        QCanvasText* q = new QCanvasText(t, this);
        q->setX(x);
        q->setY(y);
        q->setZ(20);
        q->setColor(c);
        q->show();
}

void
canvas::draw_line(int x1, int y1, int x2, int y2)
{
        QCanvasLine* l = new QCanvasLine(this);
        l->setPoints(x1, y1, x2, y2);
        l->setZ(10);
        QPen p(QColor(0, 0, 255));
        p.setWidth(2);
        l->setPen(p);
        l->show();
}

void
canvas::draw_arrow(int x1, int y1, int x2, int y2)
{
        draw_line(x1, y1, x2, y2);
        int x0 = (x1 + x2) / 2;
        int y0 = (y1 + y2) / 2;
        int x = x0 - 14 * (x2 - x1) / 
                sqrt((x2 - x1) * (x2 - x1) + (y2 - y1) * (y2 - y1));
        int y = y0 - 14 * (y2 - y1) / 
                sqrt((x2 - x1) * (x2 - x1) + (y2 - y1) * (y2 - y1));
        int dx = 7 * (y2 - y1) / 
                sqrt((x2 - x1) * (x2 - x1) + (y2 - y1) * (y2 - y1));
        int dy = 7 * (x2 - x1) / 
                sqrt((x2 - x1) * (x2 - x1) + (y2 - y1) * (y2 - y1));
        draw_line(x0, y0, x - dx, y + dy);
        draw_line(x0, y0, x + dx, y - dy);
}

void
canvas::draw_horizontal_curve(int x1, int x2, int y, bool f)
{
        int a = (x2 - x1) / 2;
        int b = (x2 - x1) / 8;
        int x0 = x1;
        int y0 = y;
        for (int x = -a; x <= a; x += 2) {
                int h = sqrt(b * b - b * b * x * x / (a * a));
                QCanvasLine* e = new QCanvasLine(this);
                int y1 = y - h;
                if(!f) {
                        y1 += 2 * h;
                }
                e->setPoints(x0, y0, x1 + a + x, y1);
                x0 = x1 + a + x;
                y0 = y1;
                e->setZ(10);
                QPen p(QColor(0, 0, 255));
                p.setWidth(2);
                e->setPen(p);
                e->show();
        }
}

void
canvas::clear()
{
        QCanvasItemList l = allItems();
        for(QCanvasItemList::iterator i = l.begin();i != l.end();++i) {
                delete *i;
        }
        update();
}

canvas::canvas(int w, int h)
                :QCanvas(w, h)
{
        setBackgroundColor(QColor(255, 255, 255));
}

}
