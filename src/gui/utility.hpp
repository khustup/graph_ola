#ifndef GUI_UTILITY_HPP
#define GUI_UTILITY_HPP
/**
 * @file gui/utility.hpp
 * @brief Interface for gui::utility class.
 */
#include <graph/graph.hpp>

namespace gui {

class canvas;

namespace utility {

        /**
         * @brief Draws graph on canvas.
         * @param c Canvas.
         * @param g Graph.
         */
        void draw_graph(canvas* c, graph::int_graph::ptr g);
}

}

#endif
