#ifndef GUI_OLA_WINDOW_HPP
#define GUI_OLA_WINDOW_HPP

/**
 * @file gui/ola_window.hpp
 * @brief Interface of gui::ola_window class.
 */
#include "canvas.hpp"

namespace graph {
class ola_line;
}

namespace gui {

/**
 * @class gui::ola_window
 * @brief Widget for drawing ola result.
 */
class ola_window: public QCanvasView
{
public:
        /**
         * @brief Draws given ola line in canvas.
         */
        void draw_ola_line(const graph::ola_line&);

protected:
        bool event(QEvent*);

private:
        gui::canvas* m_canvas;

public:
        /// @brief Constructor.
        ola_window();
};

}

#endif
