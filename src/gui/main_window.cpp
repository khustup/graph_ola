/**
 * @file gui/main_window.cpp
 * @brief Implementation of class gui::main_window
 */

#include "main_window.hpp"
#include "canvas.hpp"
#include "utility.hpp"

#include <graph/graph_generator.hpp>
#include <graph/ola_engine.hpp>
#include <graph/ola_line.hpp>
#include <graph/utility.hpp>

#include <stdlib.h>

#include <qaction.h>
#include <qdialog.h>
#include <qfiledialog.h>
#include <qlabel.h>
#include <qmenubar.h>
#include <qmessagebox.h>
#include <qpopupmenu.h>
#include <qstatusbar.h>
#include <qtoolbar.h>

namespace gui {

void
main_window::exit()
{
        ::exit(0);
}

void
main_window::open_file()
{
        m_file_selector->rereadDir();
        if (m_file_selector->exec() == QDialog::Accepted) {
                QString f;
                f = m_file_selector->selectedFile();
                try {
                        m_graph = graph::utility::import_graph<int>(f);
                        utility::draw_graph(m_canvas, m_graph);
                } catch(graph::exception& e) {
                        QMessageBox b("Error",
                                      e.what(),
                                      QMessageBox::Critical,
                                      QMessageBox::Ok, 0, 0,
                                      this);
                        b.exec();
                }
                statusBar()->message("Press Run, to run algorithm.");
        }
}

void
main_window::save_file()
{
        if(!m_graph) {
                QMessageBox b("Empy Screen",
                              "There is no active graph.",
                              QMessageBox::Warning,
                              QMessageBox::Ok, 0, 0,
                              this);
                b.exec();
                return;
        }
        m_file_saver->rereadDir();
        if (m_file_saver->exec() == QDialog::Accepted) {
                QString f;
                f = m_file_saver->selectedFile();
                graph::utility::export_graph<int>(m_graph, f);
        }
}

void
main_window::show_about()
{
        m_show_about->exec();
}

void
main_window::run()
{
        if(m_graph) {
                graph::ola_engine e(m_graph);
                e.run();
                m_ola_window->draw_ola_line(e.get_ola_line());
                m_ola_window->show();
                m_ola_window->raise();
        } else {
                QMessageBox b("Select Graph",
                              "At first select graph.",
                              QMessageBox::Warning,
                              QMessageBox::Ok, 0, 0,
                              this);
                b.exec();
        }
}

void
main_window::random_graph()
{
        graph::int_graph::ptr g = graph::graph_generator::get_instance().get(5, 20);
        m_graph = g;
        utility::draw_graph(m_canvas, m_graph);
        statusBar()->message("Press Run, to run algorithm.");
}

void
main_window::create_menubar()
{
        QMenuBar* m = menuBar();

        QPopupMenu* p = new QPopupMenu(this);
        p->insertItem("Open", this, SLOT(open_file()), Key_O);
        p->insertItem("Save As", this, SLOT(save_file()), Key_S);
        p->insertSeparator();
        p->insertItem("Exit", this, SLOT(exit()), Key_Q);
        m->insertItem("File", p);

        QPopupMenu* t = new QPopupMenu(this);
        t->insertItem("Run", this, SLOT(run()), Key_R);
        t->insertItem("Random Graph", this, SLOT(random_graph()), Key_G);
        m->insertItem("Tools", t);

        QPopupMenu* h = new QPopupMenu(this);
        h->insertItem("About Program", this, SLOT(show_about()), Key_F1);
        m->insertItem("Help", h);
}

void
main_window::create_toolbar()
{
        QToolBar* t = new QToolBar(this);
        t->show();
        addDockWindow(t);
        QAction* a = new QAction(QPixmap("open.xpm"), "Open", 
                        Key_O, this, "open");
        QObject::connect(a, SIGNAL(activated()), this, SLOT(open_file()));
        a->addTo(t);
        QAction* s = new QAction(QPixmap("save.xpm"), "Save", 
                        Key_S, this, "save");
        QObject::connect(s, SIGNAL(activated()), this, SLOT(save_file()));
        s->addTo(t);
        t->addSeparator();
        QAction* b = new QAction(QPixmap("run.xpm"), "Run", 
                        Key_R, this, "run");
        QObject::connect(b, SIGNAL(activated()), this, SLOT(run()));
        b->addTo(t);
}

void
main_window::create_status_bar()
{
        QStatusBar* s = statusBar();
        s->message("Open graph file.");
        s->show();
}

void
main_window::create_canvas()
{
        QCanvasView* v = new QCanvasView(m_canvas, this);
        setCentralWidget(v);
        v->show();
}

void
main_window::create_about_dialog()
{
        m_show_about->setCaption("About Program");
        m_show_about->setMinimumWidth(400);
        m_show_about->setMinimumHeight(70);
        QLabel* l = new QLabel(m_show_about);
        l->setTextFormat(Qt::RichText);
        l->setFixedWidth(400);
        l->setFixedHeight(70);
        l->setText("<b>Graph Optimal Linear Arrangement Tool</b><br />"
                   "<u>Author:</u> <i>Sasun Hambardzumyan</i>");
        l->show();
}

main_window::main_window(std::string t)
                : QMainWindow()
                , m_file_selector(new QFileDialog(this, "", true))
                , m_file_saver(new QFileDialog(this, "", true))
                , m_canvas(new canvas(900, 700))
                , m_show_about(new QDialog(this))
                , m_graph()
                , m_ola_window(new ola_window())
{
        m_file_selector->setMode(QFileDialog::ExistingFile);
        m_file_saver->setMode(QFileDialog::AnyFile);

        setCaption(QString(t));
        setGeometry(0, 0, 1000, 800);

        create_menubar();
        create_toolbar();
        create_status_bar();

        create_canvas();

        create_about_dialog();

        m_ola_window->setGeometry(95, 95, 810, 610);
        m_ola_window->setFixedSize(810, 410);
        m_ola_window->setCaption("Optimal Linear Arrangement");
}

main_window::~main_window()
{
        delete m_file_selector;
        delete m_file_saver;
        delete m_canvas;
        delete m_show_about;
        delete m_ola_window;
}

}
