#ifndef GUI_CANVAS_HPP
#define GUI_CANVAS_HPP

/**
 * @file gui/canvas.hpp
 * @brief Interface for class gui::canvas
 */
#include <qcanvas.h>

namespace gui {

/**
 * @class gui::canvas
 * @brief Class for drawing graph in main window.
 */
class canvas: public QCanvas
{
        /**
         * @name Drawing Interfaces.
         * @{
         */
public:
        /**
         * @brief Draws a point with given label.
         * @param x X coordinate.
         * @param y Y coordinate.
         * @param t Label of point.
         */
        void draw_point(int x, int y, QString t);

public:
        /**
         * @brief Draws a text in canvas.
         * @param x X coordinate.
         * @param y Y coordinate.
         * @param t Text.
         * @param c Color of text.
         */
        void draw_text(int x, int y, QString t, QColor c = QColor(0, 0, 0));

public:
        /**
         * @brief Draws a line.
         * @param x1 X coordinate of start point.
         * @param y1 Y coordinate of start point.
         * @param x2 X coordinate of end point.
         * @param y2 Y coordinate of end point.
         */
        void draw_line(int x1, int y1, int x2, int y2);

public:
        /**
         * @brief Draws an arrow.
         * @param x1 X coordinate of start point.
         * @param y1 Y coordinate of start point.
         * @param x2 X coordinate of end point.
         * @param y2 Y coordinate of end point.
         */
        void draw_arrow(int x1, int y1, int x2, int y2);

public:
        /**
         * @brief Draws a horizontal curve.
         * @param x1 X coordinate of start point.
         * @param x2 X coordinate of end point.
         * @param y Y coordinate of line.
         * @param b Flag identifying a direction of curve.
         */
        void draw_horizontal_curve(int x1, int x2, int y, bool b);

public:
        /// @brief Clears all the canvas.
        void clear();
        /// @}
public:
        canvas(int w, int h);
};
        
}

#endif
