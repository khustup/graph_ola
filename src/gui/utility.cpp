/**
 * @file gui/utility.hpp
 * @brief Implementation of gui::utility class.
 */

#include "utility.hpp"
#include "canvas.hpp"

#include <graph/utility.hpp>

#include <stdlib.h>

namespace gui {

namespace utility {

void draw_graph(canvas* c, graph::int_graph::ptr g)
{
        assert(c != 0);
        c->clear();
        std::list<graph::int_graph::vertices> l;
        graph::utility::get_layers<int>(g, l);
        int h = c->height();
        int w = c->width();
        int dy = h / (l.size() + 1);
        int y = dy;
        std::map<graph::int_vertex::ptr, QPoint> m;
        while(!l.empty()) {
                graph::int_graph::vertices& v = l.front();
                int dx = w / (v.size() + 1);
                int x = dx;
                graph::int_graph::vertices::const_iterator i = v.begin();
                while(i != v.end()) {
                        int rx = rand() % (2 * dx / 3) - (dx / 3);
                        m[*i] = QPoint(x + rx, y);
                        ++i;
                        x += dx;
                }
                y += dy;
                l.pop_front();
        }
        graph::int_graph::vertices::const_iterator i = g->get_vertices().begin();
        graph::int_graph::vertices::const_iterator e = --g->get_vertices().end();
        while(i != e) {
                graph::int_graph::vertices::const_iterator k = i;
                ++k;
                while(k != g->get_vertices().end()) {
                        if((*i)->is_neighbour(*k)) {
                                c->draw_arrow(m[*i].x(), m[*i].y(),
                                                m[*k].x(), m[*k].y());
                        } else if((*k)->is_neighbour(*i)) {
                                c->draw_arrow(m[*k].x(), m[*k].y(),
                                                m[*i].x(), m[*i].y());
                        }
                        ++k;
                }
                ++i;
        }
        std::map<graph::int_vertex::ptr, QPoint>::iterator k = m.begin();
        while(k != m.end()) {
                c->draw_point(k->second.x(), k->second.y(), 
                                QString::number(k->first->get_label()));
                ++k;
        }
        c->update();
}

}

}
