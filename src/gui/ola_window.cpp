/**
 * @file gui/ola_window.cpp
 * @brief Implementation of gui::ola_window class.
 */
#include "ola_window.hpp"

#include <graph/ola_line.hpp>

#include <string.h>
#include <iostream>

namespace gui {

void ola_window::
draw_ola_line(const graph::ola_line& l)
{
        m_canvas->clear();
        int y = height() / 2;
        int dx = width() / (l.get_line().size() + 1);
        graph::ola_line::line::const_iterator i = l.get_line().begin();
        std::map<graph::int_vertex::ptr, int> m;
        while(i != l.get_line().end()) {
                m[i->first] = dx * i->second;
                ++i;
        }
        graph::int_graph::vertices::const_iterator j = 
                l.get_graph()->get_vertices().begin();
        while(j != l.get_graph()->get_vertices().end()) {
                m_canvas->draw_point(m[*j], y,
                                QString::number((*j)->get_label()));
                graph::int_vertex::vertices::const_iterator k =
                        (*j)->get_neighbours().begin();
                while(k != (*j)->get_neighbours().end()) {
                        assert(m[*j] < m[*k]);
                        if(m[*j] == m[*k] - dx) {
                                m_canvas->draw_line(m[*j], y, m[*k], y);
                        } else {
                                m_canvas->draw_horizontal_curve(m[*j], m[*k], y, rand() % 2);
                        }
                        ++k;
                }
                ++j;
        }
        QString s;
        s += QChar(0x0535);
        s += QChar(0x0580);
        s += QChar(0x056F);
        s += QChar(0x0561);
        s += QChar(0x0580);
        s += QChar(0x0578);
        s += QChar(0x0582);
        s += QChar(0x0569);
        s += QChar(0x0575);
        s += QChar(0x0578);
        s += QChar(0x0582);
        s += QChar(0x0576);
        s += ": ";
        s += QString::number(l.get_result());
        m_canvas->draw_text(20, height() - 100, s);
        s = QChar(0x0533);
        s += QChar(0x0561);
        s += QChar(0x0563);
        s += QChar(0x0561);
        s += QChar(0x0569);
        s += QChar(0x0576);
        s += QChar(0x0565);
        s += QChar(0x0580);
        s += QChar(0x056b);
        s += ' ';
        s += QChar(0x0584);
        s += QChar(0x0561);
        s += QChar(0x0576);
        s += QChar(0x0561);
        s += QChar(0x056f);
        s += ": ";
        s += QString::number(l.get_graph()->get_vertices().size());
        m_canvas->draw_text(20, height() - 75, s);
        s = QChar(0x053f);
        s += QChar(0x0578);
        s += QChar(0x0572);
        s += QChar(0x0565);
        s += QChar(0x0580);
        s += QChar(0x056b);
        s += ' ';
        s += QChar(0x0584);
        s += QChar(0x0561);
        s += QChar(0x0576);
        s += QChar(0x0561);
        s += QChar(0x056f);
        s += ": ";
        s += QString::number(l.get_graph()->get_edges().size());
        m_canvas->draw_text(20, height() - 50, s);
        m_canvas->update();
}

bool
ola_window::event(QEvent* e)
{
        if(e->type() == QEvent::KeyPress) {
                QKeyEvent* k = dynamic_cast<QKeyEvent*>(e);
                assert(k != 0);
                if(k->key() == Qt::Key_Q) {
                        hide();
                        return true;
                }
        }
        return QCanvasView::event(e);
}

ola_window::ola_window()
                : QCanvasView()
                , m_canvas(new gui::canvas(800, 400))
{
        setCanvas(m_canvas);
}

}
