#ifndef GUI_MAIN_WINDOW_HPP
#define GUI_MAIN_WINDOW_HPP

/**
 * @file gui/main_window.hpp
 * @brief Interface for class gui::main_window
 */
#include "ola_window.hpp"

#include <graph/graph.hpp>

#include <qmainwindow.h>

class QFileDialog;

namespace gui {

class canvas;

/**
 * @class gui::main_window
 * @brief Class representing the main window of tool.
 */
class main_window: public QMainWindow
{
        Q_OBJECT
        /**
         * @name Implementation.
         * @{
         */
private:
        void create_menubar();
        void create_toolbar();
        void create_status_bar();
        void create_canvas();
        void create_about_dialog();

public slots:
        void exit();
        void open_file();
        void save_file();
        void show_about();
        void run();
        void random_graph();

private:
        QFileDialog* m_file_selector;
        QFileDialog* m_file_saver;
        canvas* m_canvas;
        QDialog* m_show_about;
        ola_window* m_ola_window;

private:
        graph::int_graph::ptr m_graph;
        /// @}

        /**
         * @name Special member functions.
         * @{
         */
public:
        /// @brief Constructor.
        /// @param t Title of window.
        main_window(std::string t);

public:
        virtual ~main_window();
        /// @}
};

}

#endif
