#ifndef GRAPH_EXCEPTION_HPP
#define GRAPH_EXCEPTION_HPP
/**
 * @file graph/exception.hpp
 * @brief Interfaces of exception classes.
 */
#include <string>

namespace graph {

/**
 * @class graph::exception
 * @brief Base class for graph exceptions.
 */
class exception
{
public:
        /// @brief Info string.
        virtual std::string what() = 0;
};

/**
 * @class graph::graph_is_not_transitive_oriented.
 * @brief Thrown when the operation for transitive
 * oriented graph receive not transitive oriented graph.
 */
class graph_is_not_transitive_oriented: public exception
{
public:
        //// @brief Info string.
        virtual std::string what()
        {
                return "Given graph is not transitive oriented.";
        }
};

/**
 * @class graph::invalid_file_format
 * @brief Thrown when file format is invalid.
 */
class invalid_file_format: public exception
{
public:
        //// @brief Info string.
        virtual std::string what()
        {
                return "Invalid file format.";
        }
};

}

#endif
