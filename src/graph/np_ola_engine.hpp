#ifndef GRAPH_NP_OLA_ENGINE_HPP
#define GRAPH_NP_OLA_ENGINE_HPP

/**
 * @file graph/np_ola_engine.hpp
 * @brief Interface of class graph::np_ola_engine.
 */
#include "base_ola_engine.hpp"

namespace graph {

/**
 * @class graph::np_ola_engine
 * @brief Engine for implementing non polynomial algorithm of
 * optimal linear arrangement for gamma oriented graphs.
 */
class np_ola_engine: public base_ola_engine
{
public:
        /**
         * @brief Algorithm for implementing ola.
         */
        virtual void run();

public:
        /// @brief Constructor.
        np_ola_engine(int_graph::ptr);
};

}

#endif
