/**
 * @file graph/np_ola_engine.cpp
 * @brief Implementation of class graph::np_ola_engine.
 */

#include "np_ola_engine.hpp"
#include "arrangement_generator.hpp"
#include "utility.hpp"

namespace graph {

np_ola_engine::np_ola_engine(int_graph::ptr g)
                :base_ola_engine(g)
{
        assert(utility::is_gamma_oriented<int>(g));
}

void
np_ola_engine::run()
{
        arrangement_generator g(get_graph());
        g.get_first_arrangement(m_line);
        unsigned r = m_line.get_result();
        ola_line l(get_graph());
        while(g.get_next_arrangement(l)) {
                if(l.get_result() < r) {
                        r = l.get_result();
                        m_line = l;
                }
        }
}

}
