/**
 * @file graph/graph_generator.cpp
 * @brief Implementation of graph::graph_generator class.
 */
#include "graph_generator.hpp"
#include "utility.hpp"

#include <cstdlib>
#include <ctime>

namespace graph {

graph_generator* graph_generator::s_instance;

graph_generator& 
graph_generator::get_instance()
{
        assert(s_instance != 0);
        return *s_instance;
}

void
graph_generator::initialize()
{
        assert(s_instance == 0);
        srand(time(0));
        s_instance = new graph_generator();
}

void
graph_generator::uninitialize()
{
        assert(s_instance != 0);
        delete s_instance;
        s_instance = 0;
}

int_graph::ptr 
graph_generator::get() const
{
        unsigned n = rand();
        intervals i;
        generate_intervals(i, n);
        return generate_graph(i);
}

int_graph::ptr 
graph_generator::get(unsigned n) const
{
        intervals i;
        generate_intervals(i, n);
        return generate_graph(i);
}

int_graph::ptr 
graph_generator::get(unsigned l, unsigned u) const
{
        assert(l <= u);
        unsigned r = rand() % (u - l + 1);
        unsigned n = l + r;
        intervals i;
        generate_intervals(i, n);
        return generate_graph(i);
}

void 
graph_generator::generate_intervals(graph_generator::intervals& i, unsigned n) const
{
        for(unsigned j = 1; j < n; ++j) {
                int l = rand();
                int u = rand();
                if(l > u) {
                        int t = l;
                        l = u;
                        u = t;
                }
                i.push_back(interval(l, u));
        }
}

void 
graph_generator::add_last_vertex(int_graph::ptr g, std::map<int_vertex::ptr, interval> m) const
{
        int_vertex::ptr v = int_vertex::create(g->get_vertices().size() + 1);
        g->add_vertex(v);
        if(utility::is_adjacent<int>(g)) {
                bool b = true;
                interval i;
                while(b) {
                        int l = rand();
                        int u = rand();
                        if(l > u) {
                                int t = l;
                                l = u;
                                u = t;
                        }
                        i = interval(l, u);
                        std::map<int_vertex::ptr, interval>::const_iterator j = m.begin();
                        while(j != m.end()) {
                                if(!i.intersects(j->second)) {
                                        b = false;
                                        break;
                                }
                                ++j;
                        }
                }
                std::map<int_vertex::ptr, interval>::const_iterator j = m.begin();
                while(j != m.end()) {
                        if(!i.intersects(j->second)) {
                                if(i < j->second) {
                                        g->add_edge(int_graph::edge(v, j->first));
                                } else {
                                        assert(j->second < i);
                                        g->add_edge(int_graph::edge(j->first, v));
                                }
                        }
                }
        } else {
                bool x = static_cast<bool>(rand() % 2);
                int_graph::vertices::const_iterator j = g->get_vertices().begin();
                while(j != g->get_vertices().end()) {
                        int_vertex::ptr u = *j++;
                        if(u == v) {
                                continue;
                        }
                        if((u->get_neighbours().empty() &&
                           u->get_previous_vertices().empty()) ||
                           (!u->get_neighbours().empty() &&
                           !u->get_previous_vertices().empty())) {
                                if(x) {
                                        g->add_edge(int_graph::edge(v, u));
                                } else {
                                        g->add_edge(int_graph::edge(u, v));
                                }
                        } else {
                                if(u->get_neighbours().empty()) {
                                        if(x) {
                                                g->add_edge(int_graph::edge(v, u));
                                        } else {
                                                bool y = static_cast<bool>(rand() % 2);
                                                if(y) {
                                                        g->add_edge(int_graph::edge(u, v));
                                                }
                                        }
                                } else {
                                        assert(u->get_previous_vertices().empty());
                                        if(x) {
                                                bool y = static_cast<bool>(rand() % 2);
                                                if(y) {
                                                        g->add_edge(int_graph::edge(v, u));
                                                }
                                        } else {
                                                g->add_edge(int_graph::edge(u, v));
                                        }
                                }
                        }
                }
        }
}

int_graph::ptr
graph_generator::generate_graph(const intervals& i) const
{
        int_graph::ptr g = int_graph::create();
        intervals::const_iterator j = i.begin();
        std::map<int_vertex::ptr, interval> m;
        int x = 1;
        while(j != i.end()) {
                int_vertex::ptr v = int_vertex::create(x++);
                g->add_vertex(v);
                m[v] = *j++;
        }
        int_graph::vertices::const_iterator k = g->get_vertices().begin();
        while(k != g->get_vertices().end()) {
                int_graph::vertices::const_iterator l = k;
                while(l != g->get_vertices().end()) {
                        if(!m[*k].intersects(m[*l])) {
                                if(m[*k] < m[*l]) {
                                        g->add_edge(int_graph::edge(*k, *l));
                                } else {
                                        assert(m[*l] < m[*k]);
                                        g->add_edge(int_graph::edge(*l, *k));
                                }
                        }
                        ++l;
                }
                ++k;
        }
        add_last_vertex(g, m);
        return g;
}

graph_generator::
graph_generator()
{
}

}
