/**
 * @file graph/arrangement_generator.cpp
 * @brief Implementation of class graph::arrangement_generator
 */
#include "arrangement_generator.hpp"
#include "ola_line.hpp"
#include "utility.hpp"

namespace graph {

arrangement_generator::arrangement_generator(int_graph::ptr g)
                                : m_graph(g)
{
        assert(utility::is_gamma_oriented<int>(g));
}

void
arrangement_generator::get_first_arrangement(ola_line& l)
{
        initialize_container();
        do {
                get_current_arrangement(l);
                if(l.is_correct_arrangement()) {
                        return;
                }
        } while(next());
        assert(!"There will be at least one correct arrangement");
}

bool
arrangement_generator::get_next_arrangement(ola_line& l)
{
        while(next()) {
                get_current_arrangement(l);
                if(l.is_correct_arrangement()) {
                        return true;
                }
        }
        return false;
}

void
arrangement_generator::generate_last_positions()
{
        while(true) {
                int_vertex::vertices u(m_container.back().first);
                assert(u.size() >= 2);
                if(u.size() == 2) {
                        assert(m_container.size() == m_graph->get_vertices().size() - 1);
                        return;
                }
                u.erase(*m_container.back().second);
                m_container.push_back(std::make_pair(u, u.begin()));
                m_container.back().second = m_container.back().first.begin();
        }
}

void
arrangement_generator::initialize_container()
{
        int_vertex::vertices v(m_graph->get_vertices());
        m_container.push_back(std::make_pair(v, v.begin()));
        m_container.back().second = m_container.back().first.begin();
        generate_last_positions();
}

int_vertex::vertices::const_iterator&
arrangement_generator::increment_last_iterator()
{
        assert(!m_container.empty());
        unsigned l = m_container.size() - 1;
        unsigned r = m_graph->get_vertices().size() - m_container.size();
        ++(m_container.rbegin()->second);
        while(m_container.rbegin()->second != 
                        m_container.rbegin()->first.end()) {
                int_vertex::ptr v = *(m_container.rbegin()->second);
                if(v->get_neighbours().size() <= r &&
                                v->get_previous_vertices().size() <= l) {
                        break;
                }
                ++(m_container.rbegin()->second);
        }
        return m_container.rbegin()->second;
}

bool
arrangement_generator::next()
{
        while(!m_container.empty() &&
                        increment_last_iterator() == 
                        m_container.rbegin()->first.end()) {
                m_container.pop_back();
        }
        if(m_container.empty()) {
                return false;
        }
        generate_last_positions();
        return true;
}

void
arrangement_generator::get_current_arrangement(ola_line& l) const
{
        assert(l.get_graph() == m_graph);
        l.get_line().clear();
        container::const_iterator i = m_container.begin();
        unsigned x = 1;
        while(i != m_container.end()) {
                l.get_line()[*((i++)->second)] = x++;
        }
        assert(m_container.rbegin()->first.size() == 2);
        int_vertex::vertices::const_iterator j = 
                        m_container.rbegin()->first.begin();
        if(m_container.rbegin()->second ==
                        m_container.rbegin()->first.begin()) {
                ++j;
        }
        l.get_line()[*j] = x;
        assert(l.is_arrangement());
}

}
