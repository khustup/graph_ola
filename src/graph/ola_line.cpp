/**
 * @file graph/ola_line.cpp
 * @brief Implementation of class graph::ola_line.
 */
#include "ola_line.hpp"

namespace graph {

unsigned
ola_line::phi(int_vertex::ptr v) const
{
        assert(m_graph->is_vertex(v));
        line::const_iterator i = m_line.find(v);
        assert(i != m_line.end());
        return i->second;
}

int_vertex::ptr
ola_line::phi_1(unsigned c) const
{
        line::const_iterator i = m_line.begin();
        int_vertex::ptr v;
        while(i != m_line.end()) {
                if(i->second == c) {
                        v = i->first;
                        break;
                }
                ++i;
        }
        assert(v != 0);
        assert(m_graph->is_vertex(v));
        return v;
}

ola_line::line&
ola_line::get_line()
{
        return m_line;
}

const ola_line::line&
ola_line::get_line() const
{
        return m_line;
}

unsigned
ola_line::get_result() const
{
        assert(is_correct_arrangement());
        line::const_iterator i = m_line.begin();
        unsigned s = 0;
        while(i != m_line.end()) {
                int_vertex::ptr v = i->first;
                const int_vertex::vertices& vs = v->get_neighbours();
                int_vertex::vertices::const_iterator j = vs.begin();
                while(j != vs.end()) {
                        s += (phi(*j++) - i->second);
                }
                ++i;
        }
        return s;
}

bool
ola_line::is_arrangement() const
{
        if(m_line.size() != m_graph->get_vertices().size()) {
                return false;
        }
        int_graph::vertices::const_iterator i = m_graph->get_vertices().begin();
        while(i != m_graph->get_vertices().end()) {
                if(m_line.find(*i++) == m_line.end()) {
                        return false;
                }
        }
        for(unsigned j = 1; j <= m_line.size(); ++j) {
                bool b = false;
                line::const_iterator k = m_line.begin();
                while(k != m_line.end()) {
                        if(k->second == j) {
                                if(b == true) {
                                        return false;
                                }
                                b = true;
                        }
                        ++k;
                }
                if(b == false) {
                        return false;
                }
        }
        return true;
}

bool
ola_line::is_correct_arrangement() const
{
        if(!is_arrangement()) {
                return false;
        }
        line::const_iterator k = m_line.begin();
        while(k != m_line.end()) {
                int_vertex::ptr v = k->first;
                const int_vertex::vertices& vs = v->get_neighbours();
                int_vertex::vertices::const_iterator j = vs.begin();
                while(j != vs.end()) {
                        line::const_iterator t = m_line.find(*j++);
                        assert(t != m_line.end());
                        if(t->second < k->second) {
                                return false;
                        }
                }
                ++k;
        }
        return true;
}

int_graph::ptr
ola_line::get_graph() const
{
        return m_graph;
}

const ola_line&
ola_line::operator=(const ola_line& l)
{
        if(this != &l) {
                m_graph = l.m_graph;
                m_line = l.m_line;
        }
        return *this;
}

ola_line::ola_line(int_graph::ptr g)
                : m_graph(g)
{
}

ola_line::ola_line(const ola_line& l)
                : m_line(l.m_line)
                , m_graph(l.m_graph)
{
}

ola_line::~ola_line()
{
}

}
