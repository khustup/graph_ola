/**
 * @file graph/ola_engine.cpp
 * @brief Implementation of class graph::ola_engine.
 */

#include "ola_engine.hpp"
#include "utility.hpp"

#include <functional>
#include <queue>

namespace graph {

ola_engine::ola_engine(int_graph::ptr g)
                :base_ola_engine(g)
{
        assert(utility::is_gamma_oriented<int>(g));
}

ola_engine::~ola_engine()
{
}

namespace {

class less: public std::binary_function<int_vertex::ptr, int_vertex::ptr, bool>
{
private:
        unsigned q(int_vertex::ptr v) const
        {
                return v->get_neighbours().size();
        }

        unsigned q_1(int_vertex::ptr v) const
        {
                return v->get_previous_vertices().size();
        }

public:
        bool operator() (int_vertex::ptr a, int_vertex::ptr b)
        {
                if(q(a) < q(b)) {
                        return true;
                } else if(q(a) > q(b)) {
                        return false;
                } else if(q_1(a) < q_1(b)) {
                        return true;
                } else if(q_1(a) > q_1(b)) {
                        return false;
                }
                return true;
        }
};

}

ola_line
ola_engine::get_tmp_line(unsigned s, unsigned t)
{
        assert(s < t);
        ola_line l(m_line);
        ola_line::line& m = l.get_line();
        int_vertex::ptr u = l.phi_1(t);
        for(unsigned x = t - 1; x >= s; --x) {
                m[l.phi_1(x)] = x + 1;
        }
        m[u] = s;
        return l;
}

void
ola_engine::run()
{
        ola_line::line& l = m_line.get_line();
        const int_graph::vertices& v = get_graph()->get_vertices();
        std::priority_queue<int_vertex::ptr, std::vector<int_vertex::ptr>, less>
                        qu(v.begin(), v.end(), less());
        unsigned x = 1;
        while(!qu.empty()) {
                int_vertex::ptr v = qu.top();
                qu.pop();
                l[v] = x++;
        }
        bool b = false;
        do {
                b = false;
                for(x = l.size() - 1; x >= 1; --x) {
                        int_vertex::ptr v = phi_1(x);
                        for(unsigned y = x + 1; y <= l.size(); ++y) {
                                unsigned c = phi(v);
                                int_vertex::ptr u = phi_1(y);
                                if(v->is_neighbour(u)) {
                                        continue;
                                }
                                ola_line t = get_tmp_line(c, y);
                                if(t.is_correct_arrangement() && m_line.get_result() > t.get_result()) {
                                        b = true;
                                        m_line = t;
                                }
                        }
                }
        } while(b == true);
}

}
