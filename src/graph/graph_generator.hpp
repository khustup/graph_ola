#ifndef GRAPH_GRAPH_GENERATOR_HPP
#define GRAPH_GRAPH_GENERATOR_HPP

/**
 * @file graph/graph_generator.hpp
 * @brief Interface for graph::graph_generator class.
 */
#include "graph.hpp"

#include <cassert>
#include <list>
#include <map>

namespace graph {

/**
 * @class graph::graph_generator
 * @brief Singletone class for generating random oriented graphs.
 */
class graph_generator
{
        /**
         * @name Singletone interface.
         * @{
         */
public:
        /**
         * @brief Returns singletone instance.
         */
        static graph_generator& get_instance();

public:
        /**
         * @Initializes the singletone instance.
         */
        static void initialize();

public:
        /**
         * @Uninitializes the singletone instance.
         */
        static void uninitialize();

private:
        static graph_generator* s_instance;
        /// @}

        /**
         * @name Interface for generating gamma oriented graph.
         * @{
         */
public:
        /**
         * @brief Returns random gamma oriented graph.
         * Number of vertices and edges are generated randomly.
         * @return Generated graph.
         */
        int_graph::ptr get() const;

public:
        /**
         * @brief Returns random gamma oriented graph 
         * with number of given vertices.
         * @param n Number of vertices.
         * @return Generated graph.
         */
        int_graph::ptr get(unsigned n) const;

public:
        /**
         * @brief Returns random gamma oriented graph
         * with random number of vertices in given interval.
         * @param l Minimum number of vertices.
         * @param u Maximum number of vertices.
         * @return Generated graph.
         */
        int_graph::ptr get(unsigned l, unsigned u) const;

private:
        class interval
        {
        private:
                int m_lower;
                int m_upper;

        public:
                bool intersects(const interval& r) const
                {
                        if(m_lower > r.m_upper || m_upper < r.m_lower) {
                                return false;
                        }
                        return true;
                }

        public:
                bool operator<(const interval& r) const
                {
                        assert(!intersects(r));
                        if(m_lower < r.m_lower) {
                                return true;
                        }
                        return false;
                }

        public:
                bool operator>(const interval& r) const
                {
                        assert(!intersects(r));
                        if(m_lower < r.m_lower) {
                                return false;
                        }
                        return true;
                }

        public:
                const interval& operator=(const interval& r)
                {
                        if(this != &r) {
                                m_lower = r.m_lower;
                                m_upper = r.m_upper;
                        }
                        return *this;
                }

        public:
                interval(int l = 0, int u = 0)
                                : m_lower(l)
                                , m_upper(u)
                {
                        assert(l <= u);
                }
        };

        typedef std::list<interval> intervals;

private:
        int_graph::ptr generate_graph(const intervals&) const;
        void generate_intervals(intervals&, unsigned n) const;

        /**
         * @brief Adds last random vertex to graph.
         * This is separated from other vertices generation
         * to support graph adjacency - last vertex adjacent to all other vertices.
         */
        void add_last_vertex(int_graph::ptr,
                        std::map<int_vertex::ptr, interval>) const;
        /// @}

        /**
         * @name Special member functions.
         * @{
         */
private:
        graph_generator();
};

}

#endif
