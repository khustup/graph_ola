#ifndef GRAPH_VERTEX_HPP
#define GRAPH_VERTEX_HPP

/**
 * @file graph/vertex.hpp
 * @brief Interface of graph::vertex class.
 */

#include <boost/shared_ptr.hpp>
#include <set>

namespace graph {

template<class T>
class graph;

/**
 * @class graph::vertex
 * @brief Represents vertex of oriented graph.
 */
template <class T>
class vertex
{
public:
        /**
         * @name Neighbours interface.
         * @{
         */
        typedef T label_type;
        typedef vertex<label_type> this_type;
        typedef boost::shared_ptr<this_type> ptr;
        typedef std::set<ptr> vertices;

public:
        /// @brief Access to neighbours of vertex.
        const vertices& get_neighbours() const
        {
                return m_neighbours;
        }

public:
        /// @brief Access to vertices which neighbour is this.
        const vertices& get_previous_vertices() const
        {
                return m_previous_vertices;
        }

public:
        /**
         * @brief Checks whether given vertex is neighbour.
         * @param v Vertex.
         * @return true if v is neighbour.
         */
        bool is_neighbour(ptr v) const
        {
                return m_neighbours.find(v) != m_neighbours.end();
        }

public:
        /**
         * @brief Checks whether given vertex is previous vertex.
         * @param v Vertex.
         * @return true if v is previous vertex.
         */
        bool is_previous_vertex(ptr v) const
        {
                return m_previous_vertices.find(v) != m_previous_vertices.end();
        }

private:
        /**
         * @brief Adds given vertex to neighbours.
         * @param v Vertex.
         */
        void add_neighbour(ptr v)
        {
                m_neighbours.insert(v);
        }

private:
        /**
         * @brief Removed given vertex to previous vertices.
         * @param v Vertex.
         */
        void add_previous_vertex(ptr v)
        {
                m_previous_vertices.insert(v);
        }

private:
        vertices m_neighbours;
        vertices m_previous_vertices;
        /// @}

        /**
         * @name Label.
         * @{
         */
public:
        /// @brief Access to label.
        label_type get_label() const
        {
                return m_label;
        }

        /// @brief Sets the label.
        void set_label(T& v)
        {
                m_label = v;
        }

private:
        label_type m_label;
        /// @}

        /**
         * @name Constructing interface
         * @{
         */
public:
        /**
         * @brief Creates a new vertex and returns it.
         * @param v Label for vertex.
         */
        static ptr create(const T& v = T())
        {
                return ptr(new vertex(v));
        }

private:
        vertex(const T& v)
                        :m_label(v)
        {}
        /// @}

private:
        friend class graph<label_type>;
};

typedef vertex<std::string> string_vertex;
typedef vertex<int> int_vertex;
}

#endif
