#ifndef GRAPH_UTILITY_HPP
#define GRAPH_UTILITY_HPP

/**
 * @file graph/utility.hpp
 * @brief Utility functions for oriented graphs.
 */
#include "graph.hpp"
#include "ola_line.hpp"
#include "exception.hpp"

#include <assert.h>
#include <algorithm>
#include <fstream>
#include <iostream>
#include <list>
#include <map>
#include <stack>
#include <sstream>

namespace graph {

namespace utility {

/**
 * @brief Checks whether the given graph is adjacent.
 */
template <class T>
bool is_adjacent(const typename graph<T>::ptr g)
{
        assert(g->is_valid());
        std::map<typename vertex<T>::ptr, bool> m;
        typename graph<T>::vertices::const_iterator i = g->get_vertices().begin();
        while(i != g->get_vertices().end()) {
                m[*i++] = false;
        }
        std::set<typename vertex<T>::ptr> s;
        typename std::map<typename vertex<T>::ptr, bool>::const_iterator k = m.begin();
        s.insert(k->first);
        while(!s.empty()) {
                typename vertex<T>::ptr t = *s.begin();
                m[t] = true;
                s.erase(s.begin());
                typename vertex<T>::vertices::const_iterator j = t->get_neighbours().begin();
                while(j != t->get_neighbours().end()) {
                        if(m[*j] == false) {
                                s.insert(*j);
                        }
                        ++j;
                }
                j = t->get_previous_vertices().begin();
                while(j != t->get_previous_vertices().end()) {
                        if(m[*j] == false) {
                                s.insert(*j);
                        }
                        ++j;
                }
        }
        while(k != m.end() && k->second == true) {
                ++k;
        }
        return k == m.end();
}

/**
 * @brief Checks whether the given graph is acyclic.
 * @todo Isn't implemented yet.
 */
template <class T>
bool is_acyclic(typename graph<T>::ptr g)
{
        assert(0);
        return false;
}

/**
 * @brief Checks whether the given graph is transitive oriented.
 */
template <class T>
bool is_transitive_oriented(const typename graph<T>::ptr g)
{
        assert(g->is_valid());
        typename graph<T>::vertices::const_iterator i = g->get_vertices().begin();
        while(i != g->get_vertices().end()) {
                typename vertex<T>::ptr v = *i++;
                if(v->is_neighbour(v)) {
                        return false;
                }
                typename vertex<T>::vertices::const_iterator j = v->get_neighbours().begin();
                while(j != v->get_neighbours().end()) {
                        typename vertex<T>::ptr u = *j++;
                        typename vertex<T>::vertices::const_iterator k = u->get_neighbours().begin();
                        while(k != u->get_neighbours().end()) {
                                if(!v->is_neighbour(*k++)) {
                                        return false;
                                }
                        }
                }
        }
        return true;
}

/**
 * @brief Checks whether the given graph is gamma oriented.
 */
template <class T>
bool is_gamma_oriented(const typename graph<T>::ptr g)
{
        assert(g->is_valid());
        typename graph<T>::vertices::const_iterator i = g->get_vertices().begin();
        while(i != g->get_vertices().end()) {
                typename vertex<T>::vertices::const_iterator j = i;
                typename vertex<T>::ptr v = *i++;
                ++j;
                while(j != g->get_vertices().end()) {
                        typename vertex<T>::ptr u = *j++;
                        if(!std::includes(u->get_neighbours().begin(), u->get_neighbours().end(),
                                                v->get_neighbours().begin(), v->get_neighbours().end()) &&
                           !std::includes(v->get_neighbours().begin(), v->get_neighbours().end(),
                                                u->get_neighbours().begin(), u->get_neighbours().end())) {
                                return false;
                        }
                }
        }
        return true;
}

template <class T>
void get_layers(const typename graph<T>::ptr g, std::list<typename graph<T>::vertices>& l)
{
        assert(l.empty());
        if(!is_transitive_oriented<T>(g)) {
                throw graph_is_not_transitive_oriented();
        }
        typename graph<T>::vertices vs;
        while (vs.size() != g->get_vertices().size()) {
                typename graph<T>::vertices c;
                typename graph<T>::vertices::const_iterator i = g->get_vertices().begin();
                while(i != g->get_vertices().end()) {
                        if(vs.find(*i) == vs.end() && std::includes(vs.begin(), vs.end(), 
                                        (*i)->get_previous_vertices().begin(),
                                        (*i)->get_previous_vertices().end())) {
                                c.insert(*i);
                        }
                        ++i;
                }
                std::copy(c.begin(), c.end(), std::inserter(vs, vs.begin()));
                l.push_back(c);
        }
}

template <class T>
void export_graph(const typename graph<T>::ptr g, std::string f)
{
        std::ofstream o(f.c_str());
        typename graph<T>::vertices::const_iterator i = 
                g->get_vertices().begin();
        while(i != g->get_vertices().end()) {
                o << (*i)->get_label() << ": ";
                typename vertex<T>::vertices::const_iterator j =
                        (*i)->get_neighbours().begin();
                while(j != (*i)->get_neighbours().end()) {
                        o << (*j++)->get_label() << " ";
                }
                o << "\n";
                ++i;
        }
        o.flush();
        o.close();
}

template <class T>
typename graph<T>::ptr import_graph(std::string f)
{
        typename graph<T>::ptr g = graph<T>::create();
        std::ifstream ff(f.c_str());
        std::string s;
        typename std::map<T, std::list<T> > m;
        T t;
        while(ff.good()) {
                ff >> s;
                if(s.empty()) {
                        break;
                }
                if(s[s.size() - 1] == ':') {
                        s.erase(s.size() - 1);
                        std::stringstream st;
                        st << s;
                        st >> t;
                        m[t] = std::list<T>();
                } else {
                        if(t == T()) {
                                throw invalid_file_format();
                        }
                        T q;
                        std::stringstream st;
                        st << s;
                        st >> q;
                        m[t].push_back(q);
                }
                s.clear();
        }
        typename std::map<T, typename vertex<T>::ptr> vs;
        typename std::map<T, typename std::list<T> >::const_iterator i = m.begin();
        while(i != m.end()) {
                vs[i->first] = vertex<T>::create(i->first);
                g->add_vertex(vs[i->first]);
                ++i;
        }
        i = m.begin();
        while(i != m.end()) {
                typename std::list<T>::const_iterator j = i->second.begin();
                while(j != i->second.end()) {
                        typename std::map<T, typename vertex<T>::ptr>::iterator k = vs.find(*j);
                        if(k == vs.end()) {
                                throw invalid_file_format();
                        }
                        g->add_edge(typename graph<T>::edge(vs[i->first], k->second));
                        ++j;
                }
                ++i;
        }
        return g;
}

template <class T>
void dump_graph(typename graph<T>::ptr g)
{
        std::cout << "---------------Graph Dump-------------" << std::endl;
        const typename graph<T>::vertices& vs = g->get_vertices();
        typename graph<T>::vertices::const_iterator i = vs.begin();
        while(i != vs.end()) {
                typename vertex<T>::ptr v = *i++;
                std::cout << v->get_label() << ": ";
                typename vertex<T>::vertices::const_iterator j = v->get_neighbours().begin();
                while(j != v->get_neighbours().end()) {
                        std::cout << (*j++)->get_label() << ", ";
                }
                std::cout << std::endl;
        }
        std::cout << "--------------------------------------" << std::endl;
}

template <class T>
void dump_arrangement(const ola_line& o)
{
        const ola_line::line& l = o.get_line();
        std::cout << "----------Arrangement-----------------" << std::endl;
        std::cout << "Vertex\t\tPosition" << std::endl;
        ola_line::line::const_iterator k = l.begin();
        while(k != l.end()) {
                std::cout << k->first->get_label() << "\t\t" << k->second << std::endl;
                ++k;
        }
        std::cout << "--------------------------------------" << std::endl;
        std::cout << "Result: " << o.get_result() << std::endl;
}

}

}

#endif
