#ifndef GRAPH_ARRANGEMENT_GENERATOR_HPP
#define GRAPH_ARRANGEMENT_GENERATOR_HPP

/**
 * @file graph/arrangement_generator.hpp
 * @brief Interface for class graph::arrangement_generator
 */
#include "graph.hpp"

#include <list>
#include <utility>

namespace graph {

class ola_line;

/**
 * @class graph::arrangement_generator
 * @brief Class for generating all linear arrangements for graph.
 */
class arrangement_generator
{
        /**
         * @name Implementation
         * @{
         */
public:
        /**
         * @brief Returns the first arrangement.
         * @param[out] l Line in which arrangement stored.
         */
        void get_first_arrangement(ola_line& l);

public:
        /**
         * @brief Returns the next arrangement.
         * @param[out] l Line in which arrangement stored.
         * @return false When there is no any arrangement.
         */
        bool get_next_arrangement(ola_line& l);

private:
        void generate_last_positions();
        void initialize_container();
        bool next();
        void get_current_arrangement(ola_line& l) const;

private:
        typedef std::list<std::pair<const int_vertex::vertices, 
                          int_vertex::vertices::const_iterator> > container;
        container m_container;
        int_graph::ptr m_graph;

private:
        int_vertex::vertices::const_iterator& increment_last_iterator();
        /// @}

        /**
         * @name Special member functions.
         * @{
         */
public:
        /// @brief Constructor.
        arrangement_generator(int_graph::ptr);
        /// @}

};

}
#endif
