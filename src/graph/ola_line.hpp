#ifndef GRAPH_OLA_LINE_HPP
#define GRAPH_OLA_LINE_HPP

/**
 * @file graph/ola_line.hpp
 * @brief Interface of class graph::ola_line.
 */
#include "graph.hpp"

#include <map>

namespace graph {

/**
 * @class graph::ola_line
 * @brief Represents the line of integral numbers in which graph
 * will be arranged.
 */
class ola_line
{
public:
        /**
         * @name Engine interface.
         * @{
         */
        typedef std::map<int_vertex::ptr, unsigned> line;

public:
        /// @brief Access to coordinate of given vertex.
        unsigned phi(int_vertex::ptr) const;

public:
        /// @brief Access to vertex of given coordinate.
        int_vertex::ptr phi_1(unsigned) const;

public:
        /// @brief Access to whole coordinate line.
        line& get_line();

public:
        /// @brief Access to whole coordinate line.
        const line& get_line() const;

public:
        /// @brief Access to result of algorithm.
        unsigned get_result() const;

public:
        /// @brief Check if map is arrangement.
        bool is_arrangement() const;

public:
        /// @brief Check if the arrangement is correct.
        bool is_correct_arrangement() const;

public:
        /// @brief Access to graph.
        int_graph::ptr get_graph() const;

private:
        line m_line;
        int_graph::ptr m_graph;
        /// @}

        /**
         * @name Special member functions.
         * @{
         */
public:
        /// @brief Constructor.
        ola_line(int_graph::ptr);

public:
        /// @brief Copy Constructor.
        ola_line(const ola_line&);

public:
        /// @brief Assignment Operator.
        const ola_line& operator=(const ola_line&);

public:
        /// @brief Destructor.
        virtual ~ola_line();
        /// @}
};

}

#endif
