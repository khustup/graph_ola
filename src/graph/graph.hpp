#ifndef GRAPH_GRAPH_HPP
#define GRAPH_GRAPH_HPP

/**
 * @file graph/graph.hpp
 * @brief Interface of graph::graph class.
 */
#include "vertex.hpp"

#include <assert.h>
#include <boost/shared_ptr.hpp>
#include <set>
#include <utility>

namespace graph {

/**
 * @class graph::graph
 * @brief Represents oriented graph.
 */
template <class T>
class graph
{
public:
        /**
         * @name Graph interface.
         * @{
         */
        typedef T value_type;
        typedef graph<value_type> this_type;
        typedef boost::shared_ptr<this_type> ptr;
        typedef vertex<value_type> vertex_type;
        typedef std::pair<typename vertex<value_type>::ptr, 
                          typename vertex<value_type>::ptr> edge;
        typedef std::set<edge> edges;
        typedef std::set<typename vertex_type::ptr> vertices;

public:
        /// @brief Access to vertices.
        const vertices& get_vertices() const
        {
                return m_vertices;
        }

public:
        /// @brief Access to edges.
        edges get_edges() const
        {
                edges e;
                typename vertices::const_iterator i = m_vertices.begin();
                while(i != m_vertices.end()) {
                        typename vertex_type::vertices::const_iterator j = 
                                (*i)->get_neighbours().begin();
                        while(j != (*i)->get_neighbours().end()) {
                                e.insert(std::make_pair(*i, *j));
                                ++j;
                        }
                        ++i;
                }
                return e;
        }

public:
        /**
         * @brief Adds the new vertex to graph.
         * @param v Vertex to add.
         */
        void add_vertex(typename vertex_type::ptr v)
        {
                m_vertices.insert(v);
        }

public:
        /**
         * @brief Adds the edge to graph.
         * @param e Edge to add.
         */
        void add_edge(const edge& e)
        {
                assert(is_vertex(e.first));
                assert(is_vertex(e.second));
                e.first->add_neighbour(e.second);
                e.second->add_previous_vertex(e.first);
        }

public:
        /**
         * @brief Checks whether given is vertex of graph.
         * @param v Vertex.
         * @return true if given vertex is vertex of graph.
         */
        bool is_vertex(typename vertex_type::ptr v) const
        {
                return m_vertices.find(v) != m_vertices.end();
        }

public:
        /**
         * @brief Checks whether graph is constructed valid.
         * @return true if graph is valid.
         * @note Complexity O(V^2)
         */
        bool is_valid() const;

private:
        vertices m_vertices;
        /// @}

        /**
         * @name Constructing.
         * @{
         */
public:
        /**
         * @brief Craetes new graph.
         */
        static ptr create()
        {
                return ptr(new graph());
        }

private:
        graph()
        {}
        /// @}
};

template <class T>
bool
graph<T>::is_valid() const
{
        typename vertices::const_iterator i = m_vertices.begin();
        while(i != m_vertices.end()) {
                typename vertex_type::ptr v = *i++;
                typename vertex_type::vertices::const_iterator j = v->get_neighbours().begin();
                while(j != v->get_neighbours().end()) {
                        typename vertex_type::ptr u = *j++;
                        if(!is_vertex(u)) {
                                return false;
                        }
                        if(!u->is_previous_vertex(v)) {
                                return false;
                        }
                }
                j = v->get_previous_vertices().begin();
                while(j != v->get_previous_vertices().end()) {
                        typename vertex_type::ptr u = *j++;
                        if(!is_vertex(u)) {
                                return false;
                        }
                        if(!u->is_neighbour(v)) {
                                return false;
                        }
                }
        }
        return true;
}

typedef graph<std::string> string_graph;
typedef graph<int> int_graph;

}

#endif
