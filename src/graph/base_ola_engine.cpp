/**
 * @file graph/base_ola_engine.cpp
 * @brief Implementation of class graph::base_ola_engine.
 */
#include "base_ola_engine.hpp"

namespace graph {

unsigned
base_ola_engine::phi(int_vertex::ptr v) const
{
        return m_line.phi(v);
}

int_vertex::ptr
base_ola_engine::phi_1(unsigned c) const
{
        return m_line.phi_1(c);
}

const ola_line&
base_ola_engine::get_ola_line() const
{
        assert(is_correct_arrangement());
        return m_line;
}

unsigned
base_ola_engine::get_result() const
{
        return m_line.get_result();
}

bool
base_ola_engine::is_correct_arrangement() const
{
        return m_line.is_correct_arrangement();
}

int_graph::ptr
base_ola_engine::get_graph()
{
        return m_line.get_graph();
}

base_ola_engine::base_ola_engine(int_graph::ptr g)
                : m_line(g)
{
}

base_ola_engine::~base_ola_engine()
{
}

}
