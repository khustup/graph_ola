#ifndef GRAPH_OLA_ENGINE_HPP
#define GRAPH_OLA_ENGINE_HPP

/**
 * @file graph/ola_engine.hpp
 * @brief Interface of class graph::ola_engine.
 */
#include "base_ola_engine.hpp"

namespace graph {

/**
 * @class graph::ola_engine
 * @brief Engine for implementing of optimal linear arrangement
 * for gamma oriented graphs.
 */
class ola_engine: public base_ola_engine
{
public:
        /**
         * @brief Algorithm for implementing ola.
         */
        virtual void run();

private:
        ola_line get_tmp_line(unsigned, unsigned);

public:
        /// @brief Constructor.
        ola_engine(int_graph::ptr);

public:
        /// @brief Destructor.
        virtual ~ola_engine();
};

}

#endif
