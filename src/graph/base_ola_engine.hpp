#ifndef GRAPH_BASE_OLA_ENGINE_HPP
#define GRAPH_BASE_OLA_ENGINE_HPP

/**
 * @file graph/base_ola_engine.hpp
 * @brief Interface of class graph::base_ola_engine.
 */
#include "graph.hpp"
#include "ola_line.hpp"

namespace graph {

/**
 * @class graph::base_ola_engine
 * @brief Abstract parent class for all engines implementing
 * optimal linear arrangement algorithm for transitive oriented graphs.
 */
class base_ola_engine
{
public:
        /**
         * @name Engine interface.
         * @{
         */

        /// @brief Access to coordinate of given vertex.
        unsigned phi(int_vertex::ptr) const;

public:
        /// @brief Access to vertex of given coordinate.
        int_vertex::ptr phi_1(unsigned) const;

public:
        /// @brief Access to whole coordinate line.
        const ola_line& get_ola_line() const;

public:
        /// @brief Access to result of algorithm.
        unsigned get_result() const;

public:
        /// @brief Check if the arrangement is correct.
        bool is_correct_arrangement() const;

public:
        /**
         * @brief Algorithm for ola problem.
         * Each concrete engine will implement algorithm in this function.
         */
        virtual void run() = 0;

public:
        /// @brief Access to graph.
        int_graph::ptr get_graph();

protected:
        ola_line m_line;
        /// @}

        /**
         * @name Special member functions.
         * @{
         */
public:
        /// @brief Constructor.
        base_ola_engine(int_graph::ptr);

public:
        /// @brief Destructor.
        virtual ~base_ola_engine();
        /// @}
};

}

#endif
